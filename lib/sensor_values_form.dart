import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class SensorValuesForm extends StatefulWidget {
  const SensorValuesForm({Key? key}) : super(key: key);

  @override
  _SensorValuesFormState createState() => _SensorValuesFormState();
}

class _SensorValuesFormState extends State<SensorValuesForm> {
  final _formKey = GlobalKey<FormState>();
  String ammoniaValueStr = '';
  String doValueStr = '';
  String phValueStr = '';

  String status = '';

  final urlStr = 'http://192.168.29.124:5000';

  Future<Map<String, dynamic>> getAPImsg() async {
    Uri url = Uri.parse('$urlStr/api');
    final response = await http.get(url);
    final data = json.decode(response.body);

    return {
      'statusCode': response.statusCode,
      'msg': data['msg'],
    };
  }

  Future<Map<String, dynamic>> getData(int id) async {
    Uri url = Uri.parse('$urlStr/api/sensor_values/$id');
    final response = await http.get(url);
    final data = json.decode(response.body);

    return {
      'statusCode': response.statusCode,
      'ammonia': data['pham,monia'],
      'do': data['phdo,'],
      'ph': data['ph'],
    };
  }

  Future<Map<String, dynamic>> setData(
      double ammoniaValue, double doValue, double phValue) async {
    Uri url = Uri.parse('$urlStr/api/sensor_values');
    final response = await http.post(url,
        headers: {
          "Content-Type": "application/json",
        },
        body: json.encode(
          {
            'ammonia': ammoniaValue,
            'do': doValue,
            'ph': phValue,
          },
        ));

    if (response.statusCode == 200) {
      final data = json.decode(response.body);
      return {
        'statusCode': response.statusCode,
        'status': data['status'],
      };
    } else {
      return {
        'statusCode': response.statusCode,
        'status': 'got some error, not server issue.',
      };
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextFormField(
                decoration: const InputDecoration(
                  hintText: 'Ammonia',
                ),
                onChanged: (value) => setState(() => ammoniaValueStr = value),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "please enter ammonia value";
                  } else if (double.tryParse(value) == null) {
                    return "enter a number please";
                  }
                  return null;
                },
              ),
              TextFormField(
                decoration: const InputDecoration(
                  hintText: 'D.O.',
                ),
                onChanged: (value) => setState(() => doValueStr = value),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "please enter D.O. value";
                  } else if (double.tryParse(value) == null) {
                    return "enter a number please";
                  }
                  return null;
                },
              ),
              TextFormField(
                decoration: const InputDecoration(
                  hintText: 'pH',
                ),
                onChanged: (value) => setState(() => phValueStr = value),
                validator: (String? value) {
                  if (value == null || value.isEmpty) {
                    return "please enter pH value";
                  } else if (double.tryParse(value) == null) {
                    return "enter a number please";
                  }
                  return null;
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: ElevatedButton(
                  onPressed: () async {
                    // Validate will return true if the form is valid, or false if
                    // the form is invalid.

                    if (_formKey.currentState!.validate()) {
                      print('button pressed');
                      Fluttertoast.showToast(
                        msg: "post request sent",
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.TOP,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.blue[400],
                        textColor: Colors.white,
                        fontSize: 16.0,
                      );

                      double ammoniaValue = double.parse(ammoniaValueStr);
                      double doValue = double.parse(doValueStr);
                      double phValue = double.parse(phValueStr);

                      Map<String, dynamic> statusMsg =
                          await setData(ammoniaValue, doValue, phValue);

                      setState(() => status =
                          'status code: ${statusMsg['statusCode']}, status: ${statusMsg['status'] == 0 ? "success" : "failed"}, status code: ${statusMsg['status']}');
                    }
                  },
                  child: const Text('Submit'),
                ),
              ),
              Text(status.isEmpty ? "waiting..." : status),
            ],
          )),
    );
  }
}
